package de.blum.repository;

import de.blum.domain.InviteCode;
import org.springframework.data.repository.CrudRepository;

public interface InviteCodeRepository extends CrudRepository<InviteCode, String> {
}
