package de.blum.domain;

import javax.persistence.*;

@Entity
public class InviteCode {
    @Id
    @Column(unique = true)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
